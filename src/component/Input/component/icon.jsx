import React from 'react';

export const ICON = { LOADER: 'loader', LOOP: 'loop', CLOSE: 'close' };

export const Loader = () => <i className="ic-loader size22 typeRed ** icon-loader" />;
export const Loop = () => <div className="loop" />;
export const Close = () => <div className="close" />;

const Icon = (icon) => {
	switch (icon) {
	case ICON.LOADER:
		return Loader();
	case ICON.LOOP:
		return Loop();
	case ICON.CLOSE:
		return Close();
	default:
		return null;
	}
};

export default Icon;
