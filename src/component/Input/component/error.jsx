import React from 'react';
import PropTypes from 'prop-types';

const Error = (errorL, errorR) => {
	if (errorL) {
		return <span className="ui-errorTag itemPosLArr">{errorL}</span>;
	}

	if (errorR) {
		return <span className="ui-errorTag itemPosRArr">{errorR}</span>;
	}

	return null;
};

Error.propTypes = { errorL: PropTypes.string, errorR: PropTypes.string };
Error.defaultProps = { errorL: undefined, errorR: undefined };

export default Error;
