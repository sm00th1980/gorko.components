import React from 'react';

const SubLabel = (subLabel) => {
	if (subLabel) {
		return <span className="flexForm-input-label">{subLabel}</span>;
	}

	return null;
};
export default SubLabel;
