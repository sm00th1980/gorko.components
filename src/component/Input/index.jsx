/* eslint "react/forbid-prop-types": "off" */

import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { uniqueId, withoutUndefined } from '../helpers/helper/';
import { wrapperKlassName, inputProps } from './helper';
import Error from './component/error';
import Icon, { ICON } from './component/icon';
import SubLabel from './component/subLabel';

const Input = ({
		children, link, groupID, value, placeholder, error, errorL, errorR, disabled,
		small, klass, onChange, generatorID = uniqueId, subLabel, icon, style,
	}) => {
	const id = generatorID.next();

	const showError = error || errorL || errorR;
	const props = inputProps(link, value, placeholder, disabled, small,	onChange, id, groupID);

	const wrapperProps = withoutUndefined({ className: wrapperKlassName(klass, showError), style });

	return (
		<div {...wrapperProps}>
			{SubLabel(subLabel)}
			<input {...props} />
			{Icon(icon)}
			{Error(errorL, errorR)}
			{children}
		</div>
	);
};

Input.propTypes = {
	link: PropTypes.string,
	groupID: PropTypes.string,
	value: PropTypes.string,
	error: PropTypes.bool,
	errorL: PropTypes.string,
	errorR: PropTypes.string,
	disabled: PropTypes.bool,
	small: PropTypes.bool,
	placeholder: PropTypes.string,
	klass: PropTypes.string,
	onChange: PropTypes.func,
	generatorID: PropTypes.shape({
		next: PropTypes.func.isRequired,
	}),
	subLabel: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.bool,
	]),
	icon: PropTypes.oneOf([
		ICON.LOADER,
		ICON.LOOP,
		ICON.CLOSE,
	]),
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.array,
	]),
	style: PropTypes.object,
};

Input.defaultProps = {
	value: '',
	link: undefined,
	groupID: _.uniqueId(),
	placeholder: ' ',
	error: false,
	errorL: undefined,
	errorR: undefined,
	disabled: false,
	small: false,
	klass: '',
	onChange: undefined,
	generatorID: uniqueId,
	subLabel: undefined,
	icon: undefined,
	children: undefined,
	style: undefined,
};

export default Input;

// examples
// без суб-метки вообще
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" small />

// генерируется пустой span без текcта
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" small subLabel />

// генерируется span и текст
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" small subLabel="text" />

// с лоадером внутри
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" icon="loader" />

// с loop внутри
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" icon="loop" />

// с close внутри
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" icon="close" />

// с простой ошибкой
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" error />

// ошибка слева
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" errorL="error" />

// ошибка справа
// <Input value="22.08.2016" link="filter" klass="** widthFlex-200" errorR="error" />
