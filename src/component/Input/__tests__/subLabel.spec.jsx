import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Input from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Input {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Input/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct input with link and subLabel', () => {
		const props = {
			link: 'filter',
			generatorID,
			subLabel: 'Cамарская область, Россия',
			groupID: '2',
		};
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<span className="flexForm-input-label">Cамарская область, Россия</span>
				<input
					type="text"
					className="flexForm-input-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue=""
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct input with link and blank subLabel', () => {
		const props = {
			link: 'filter',
			generatorID,
			subLabel: true,
			groupID: '2',
		};
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<span className="flexForm-input-label" />
				<input
					type="text"
					className="flexForm-input-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue=""
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});
});
