import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Input from '../../';
import { Close, ICON } from '../../component/icon';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Input {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Input/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct input with link and icon=close', () => {
		const props = {
			link: 'filter',
			generatorID,
			groupID: '2',
			icon: ICON.CLOSE,
		};
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<input
					type="text"
					className="flexForm-input-area"
					id={`baseId-${props.link}-01`}
					name={`nameId-${props.link}-${props.groupID}`}
					aria-labelledby={`ariaId-${props.link}-${props.groupID}`}
					defaultValue=""
					placeholder=" "
				/>
				{ Close() }
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});
});
