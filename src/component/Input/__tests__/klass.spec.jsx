import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Input from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Input {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Input/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct input with link and klass', () => {
		const props = { link: 'filter', klass: '** widthFlex-200', generatorID, groupID: '2' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input ** widthFlex-200">
				<input
					type="text"
					className="flexForm-input-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue=""
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});
});
