import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Input from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Input {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Input/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct input with link and small-size', () => {
		const props = { link: 'filter', small: true, generatorID, groupID: '2' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<input
					type="text"
					className="flexForm-input-area typeSmall"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue=""
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct input with link and value and small-size', () => {
		const props = { link: 'filter', value: 'Text', small: true, generatorID, groupID: '2' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<input
					type="text"
					className="flexForm-input-area typeSmall"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue="Text"
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct input with link and value and error and small-size', () => {
		const props = { link: 'filter', value: 'Text', error: true, small: true, generatorID, groupID: '2' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input typeError">
				<input
					type="text"
					className="flexForm-input-area typeSmall"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue="Text"
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct input with link and value and disabled and small-size', () => {
		const props = { link: 'filter', value: 'Text', disabled: true, small: true, generatorID, groupID: '2' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<input
					type="text"
					className="flexForm-input-area typeSmall"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue="Text"
					placeholder=" "
					disabled
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});
});
