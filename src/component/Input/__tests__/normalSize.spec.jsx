import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Input from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Input {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Input/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct input with link', () => {
		const props = { link: 'filter', generatorID, groupID: '2' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<input
					type="text"
					className="flexForm-input-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue=""
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct input with link and value', () => {
		const props = { link: 'filter', value: 'value', generatorID, groupID: '2' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flexForm-input">
				<input
					type="text"
					className="flexForm-input-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultValue="value"
					placeholder=" "
				/>
			</div>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});
});
