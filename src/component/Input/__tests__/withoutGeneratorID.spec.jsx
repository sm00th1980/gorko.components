import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Input from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Input {...props} />);
	return renderer.getRenderOutput();
};

describe('component/Input/', () => {
	it('should render radio without generatorID', () => {
		const props = { link: 'filter', groupID: '2' };
		expect(() => create(props)).not.toThrow();
	});
});
