import classNames from 'classnames';
import _ from 'lodash';

const inputKlassName = small => classNames(
		'flexForm-input-area',
	{
		typeSmall: small,
	},
);

export const wrapperKlassName = (klass, showError) => classNames(
		'flexForm-input',
	{
		[`${klass}`]: klass,
		typeError: showError,
	},
);

export const inputProps = (link, value, placeholder, disabled, small, onChange, id, groupID) => {
	let baseProps = {
		className: inputKlassName(small),
		type: 'text',
		id: `baseId-${link}-${id}`,
		name: `nameId-${link}-${groupID}`,
		'aria-labelledby': `ariaId-${link}-${groupID}`,
		placeholder,
	};

	baseProps = disabled ? { ...baseProps, disabled } : baseProps;
	return onChange
		? _.extend(baseProps, { value, onChange })
		: _.extend(baseProps, { defaultValue: value });
};
