import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import ModalTitle from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<ModalTitle {...props} />);

	return renderer.getRenderOutput();
};

describe('component/ModalTitle/', () => {
	it('should display with style', () => {
		const props = { value: 'text', style: { justifyContent: 'center' } };
		const actualElement = create(props);
		const expectedElement = (
			<div className="modal-title" style={props.style}>
				{ props.value}
				<i className="ic-close size20 ** icon-close" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
