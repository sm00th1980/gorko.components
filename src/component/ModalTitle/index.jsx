/* eslint "react/forbid-prop-types": "off" */

import React from 'react';
import PropTypes from 'prop-types';
import { withoutUndefined } from '../helpers/helper/';

const ModalTitle = ({ value, style }) => {
	const props = withoutUndefined({ className: 'modal-title', style });

	return (
		<div {...props}>
			{ value }
			<i className="ic-close size20 ** icon-close" />
		</div>
	);
};

ModalTitle.propTypes = {
	value: PropTypes.string.isRequired,
	style: PropTypes.object,
};

ModalTitle.defaultProps = {
	style: undefined,
};

export default ModalTitle;

// examples
// <ModalTitle value={t('Вход на Горько!')} />
