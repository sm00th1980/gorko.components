import _ from 'lodash';
import classNames from 'classnames';
import Input from '../../Input/';
import Check from '../../Check/';
import Radio from '../../Radio/';
import Select from '../../Select/';

export const wrapperKlassName = (error, additionalKlass) => classNames(
	'form-check',
	{
		[`${additionalKlass}`]: additionalKlass,
		typeError: error,
	},
);

export const inputProps = type => ({ link, checked, disabled, id, groupID, onChange }) => {
	let baseProps = {
		type,
		className: 'form-check-area',
		id: `labelId-${link}-${id}`,
		name: `nameId-${link}-${groupID}`,
		'aria-labelledby': `ariaId-${link}-${groupID}`,
	};

	baseProps = disabled ? { ...baseProps, disabled } : baseProps;
	return onChange
		? { ...baseProps, checked, onChange }
		: { ...baseProps, defaultChecked: checked };
};

export const uniqueId = { next: _.uniqueId };

export const withoutUndefined = obj => _.omitBy(obj, value => _.isUndefined(value));

export const isOneOfType = (types, component) => {
	if (component && component.type) {
		return types.reduce((memo, _type) => (memo ? true : component.type === _type), false);
	}

	return false;
};

export const elementNeedLink = element => isOneOfType([Input, Check, Radio, Select], element);

export const filterChildren = (children) => {
	if (children && !_.isArray(children)) {
		return children;
	}

	if (_.isArray(children) && _.compact(children).length > 0) {
		return _.compact(children);
	}

	return null;
};
