import React from 'react';
import { filterChildren } from '../';

describe('Helper - .filterChildren', () => {
	it('should reject invalid children', () => {
		expect(filterChildren(null)).toBe(null);
		expect(filterChildren(undefined)).toBe(null);
	});

	it('should pass valid children', () => {
		expect(filterChildren(<div />)).toEqual(<div />);
	});

	it('should reject invalid children as array', () => {
		expect(filterChildren([])).toBe(null);
	});

	it('should pass only valid children as array', () => {
		expect(filterChildren([null, <div />])).toEqual([<div />]);
	});
});
