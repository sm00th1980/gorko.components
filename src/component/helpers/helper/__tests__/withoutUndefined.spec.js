import { withoutUndefined } from '../';

describe('Helper - .withoutUndefined', () => {
	it('should reject undefined values', () => {
		expect(withoutUndefined({ test: 1 })).toEqual({ test: 1 });
		expect(withoutUndefined({ test: 1, style: undefined })).toEqual({ test: 1 });
		expect(withoutUndefined({ className: 'form-check', style: undefined })).toEqual({ className: 'form-check' });
		expect(withoutUndefined({ style: undefined })).toEqual({});
	});
});
