import { isOneOfType } from '../';
import Input from '../../../Input/';
import Check from '../../../Check/';
import Radio from '../../../Radio/';
import Select from '../../../Select/';

describe('Helper - .isOneOfType', () => {
	it('should return false if component is invalid', () => {
		expect(isOneOfType([Input, Check, Radio, Select], undefined)).toBe(false);
		expect(isOneOfType([Input, Check, Radio, Select], null)).toBe(false);
	});

	it('should return true if component in list', () => {
		expect(isOneOfType([Input, Check, Radio, Select], { type: Input })).toBe(true);
		expect(isOneOfType([Input, Check, Radio, Select], { type: Check })).toBe(true);
		expect(isOneOfType([Input, Check, Radio, Select], { type: Radio })).toBe(true);
		expect(isOneOfType([Input, Check, Radio, Select], { type: Select })).toBe(true);
	});
});
