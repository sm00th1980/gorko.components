import Helper, { SEPARATOR, OCTET } from '../';

const klass = 'activeClass';
const octet = OCTET.FIRST;

describe('Helper toggle klass for normal inputs', () => {
	it('should add klass into specific octet', () => {
		const sourceKlass = `widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

		const element = document.createElement('div');
		element.className = sourceKlass;

		Helper.addClass(klass).intoOctet(octet).applyToElement(element);
		expect(element.className).toEqual(Helper.withSpecialKlass(sourceKlass, klass, octet));
	});

	it('should remove klass before first separator', () => {
		const sourceKlass = `widget-iconArrowLH size15 ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

		const element = document.createElement('div');
		element.className = sourceKlass;

		Helper.removeClass(klass).intoOctet(octet).applyToElement(element);
		expect(element.className).toEqual(Helper.withoutSpecialKlass(sourceKlass, klass, octet));
	});
});
