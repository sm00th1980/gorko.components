import Helper, { SEPARATOR, OCTET } from '../';

const klass = 'activeClass';
const octet = OCTET.FIRST;

describe('Helper toggle klass for normal inputs - use .when', () => {
	it('should add klass before first separator', () => {
		const sourceKlass = `widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

		const element = document.createElement('div');
		element.className = sourceKlass;

		Helper.toggleClass(klass).intoOctet(octet).applyToElement(element).when(true);
		expect(element.className).toEqual(Helper.withSpecialKlass(sourceKlass, klass, octet));
	});

	it('should remove klass before first separator', () => {
		const sourceKlass = `widget-iconArrowLH size15 ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

		const element = document.createElement('div');
		element.className = sourceKlass;

		Helper.toggleClass(klass).intoOctet(octet).applyToElement(element).when(false);
		expect(element.className).toEqual(Helper.withoutSpecialKlass(sourceKlass, klass, octet));
	});
});

describe('Helper toggle klass for normal inputs  - use .unless', () => {
	it('should add klass before first separator', () => {
		const sourceKlass = `widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

		const element = document.createElement('div');
		element.className = sourceKlass;

		Helper.toggleClass(klass).intoOctet(octet).applyToElement(element).unless(false);
		expect(element.className).toEqual(Helper.withSpecialKlass(sourceKlass, klass, octet));
	});

	it('should remove klass before first separator', () => {
		const sourceKlass = `widget-iconArrowLH size15 ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

		const element = document.createElement('div');
		element.className = sourceKlass;

		Helper.toggleClass(klass).intoOctet(octet).applyToElement(element).unless(true);
		expect(element.className).toEqual(Helper.withoutSpecialKlass(sourceKlass, klass, octet));
	});
});
