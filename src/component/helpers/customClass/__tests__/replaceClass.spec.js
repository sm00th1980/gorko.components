import Helper, { SEPARATOR, OCTET } from '../';

const prevKlass = 'activeClass_1';
const nextKlass = 'activeClass_2';
const octet = OCTET.FIRST;

describe('Helper replace klass', () => {
	it('should replace klass', () => {
		const sourceKlass = `widget-iconArrowLH size15 ${prevKlass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

		const element = document.createElement('div');
		element.className = sourceKlass;

		Helper.replaceClass(prevKlass).toClass(nextKlass).intoOctet(octet).applyToElement(element);

		expect(element.className).toEqual(
			`widget-iconArrowLH size15 ${nextKlass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`,
		);
	});
});
