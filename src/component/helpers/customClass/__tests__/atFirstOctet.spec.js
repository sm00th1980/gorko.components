import Helper, { SEPARATOR, OCTET } from '../';

const klass = 'nonActiveClass';
const octet = OCTET.FIRST;

describe('Helper adding/remove klass for normal inputs', () => {
	describe('withSpecialKlass', () => {
		it('should add klass before first separator', () => {
			const source = `widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

			expect(Helper.withSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowLH size15 ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`);
		});

		it('should add klass before first separator', () => {
			const source = `widget-iconArrowLH size15 ${klass} ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

			expect(Helper.withSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowLH size15 ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`);
		});
	});

	describe('withoutSpecialKlass', () => {
		it('should remove klass before first separator', () => {
			const source = `widget-iconArrowLH size15 ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`);
		});

		it('should remove klass if klass inserted 2 times', () => {
			const source = `widget-iconArrowLH size15 ${klass} ${klass} ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH`);
		});

		it('should not remove klass if klass inserted at second octet', () => {
			const source = `widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${klass} ${SEPARATOR} icon-arrowLH`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(source);
		});

		it('should not remove klass if klass inserted at third octet', () => {
			const source = `widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${SEPARATOR} icon-arrowLH ${klass}`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(source);
		});

		it('should not remove klass if klass inserted at second and third octet', () => {
			const source = `widget-iconArrowLH size15 ${SEPARATOR} -sectAlbum-gallery-strip ${klass} ${SEPARATOR} icon-arrowLH ${klass}`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(source);
		});
	});
});

describe('Helper adding/remove klass for invalid inputs', () => {
	it('should return empty string if source is undefined', () => {
		const source = undefined;

		expect(Helper.withSpecialKlass(source, klass, octet)).toEqual('');
	});

	it('should return empty string if source is undefined', () => {
		const source = undefined;

		expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual('');
	});
});
