import Helper, { SEPARATOR, OCTET } from '../';

const klass = '-sectAlbum-gallery-arrow-hidden';
const octet = OCTET.SECOND;

describe('Helper adding/remove special klass for normal inputs', () => {
	describe('withSpecialKlass', () => {
		it('should add klass before second separator', () => {
			const source = `widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${SEPARATOR} icon-arrowRH`;

			expect(Helper.withSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${klass} ${SEPARATOR} icon-arrowRH`);
		});

		it('should add klass before second separator', () => {
			const source = `widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${klass} ${klass} ${SEPARATOR} icon-arrowRH`;

			expect(Helper.withSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${klass} ${SEPARATOR} icon-arrowRH`);
		});
	});

	describe('withoutSpecialKlass', () => {
		it('should remove klass before first separator', () => {
			const source = `widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${klass} ${SEPARATOR} icon-arrowRH`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${SEPARATOR} icon-arrowRH`);
		});

		it('should remove klass if klass inserted 2 times', () => {
			const source = `widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} ${klass} -sectAlbum-gallery-arrow-r ${klass} ${SEPARATOR} icon-arrowRH`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(`widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${SEPARATOR} icon-arrowRH`);
		});

		it('should not remove klass if klass at first octet', () => {
			const source = `widget-iconArrowRH size52 widget-iconBg80-20 ${klass} ${SEPARATOR} -sectAlbum-gallery-arrow-r ${SEPARATOR} icon-arrowRH`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(source);
		});

		it('should not remove klass if klass at third octet', () => {
			const source = `widget-iconArrowRH size52 widget-iconBg80-20 ${SEPARATOR} -sectAlbum-gallery-arrow-r ${SEPARATOR} icon-arrowRH ${klass}`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(source);
		});

		it('should not remove klass if klass at first and third octet', () => {
			const source = `widget-iconArrowRH size52 widget-iconBg80-20 ${klass} ${SEPARATOR} -sectAlbum-gallery-arrow-r ${SEPARATOR} icon-arrowRH ${klass}`;

			expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual(source);
		});
	});
});

describe('Helper adding/remove klass for invalid inputs', () => {
	it('should return empty string if source is undefined', () => {
		const source = undefined;

		expect(Helper.withSpecialKlass(source, klass, octet)).toEqual('');
	});

	it('should return empty string if source is undefined', () => {
		const source = undefined;

		expect(Helper.withoutSpecialKlass(source, klass, octet)).toEqual('');
	});
});
