import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Select from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Select {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Select/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct small select with link', () => {
		const props = {
			link: 'filter',
			small: true,
			items: [
				{ value: 'value1', title: 'title1' },
				{ value: 'value2', title: 'title2' },
			],
			generatorID,
			groupID: '2',
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="flexForm-select typeSmall">
				<select
					className="flexForm-select-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					required
					defaultValue="value1"
				>
					<option value="value1">title1</option>
					<option value="value2">title2</option>
				</select>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct small select with link and klass', () => {
		const props = {
			link: 'filter',
			small: true,
			klass: '** widthFlex-200',
			items: [
				{ value: 'value1', title: 'title1' },
				{ value: 'value2', title: 'title2' },
			],
			generatorID,
			groupID: '2',
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="flexForm-select typeSmall ** widthFlex-200">
				<select
					className="flexForm-select-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					required
					defaultValue="value1"
				>
					<option value="value1">title1</option>
					<option value="value2">title2</option>
				</select>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
