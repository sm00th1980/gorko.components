import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Select from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Select {...props} />);
	return renderer.getRenderOutput();
};

describe('component/Select/', () => {
	it('should render radio without generatorID', () => {
		const props = {
			link: 'filter',
			items: [
				{ value: 'value1', title: 'title1' },
				{ value: 'value2', title: 'title2' },
			],
			groupID: '2',
		};
		expect(() => create(props)).not.toThrow();
	});
});
