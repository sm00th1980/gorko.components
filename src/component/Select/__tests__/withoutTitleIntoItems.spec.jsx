import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Select from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Select {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Select/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct select with link and value and without title into items', () => {
		const props = {
			link: 'filter',
			items: [
				{ value: 'value1', title: 'title1' },
				{ value: 'value2' },
				{ value: 'value3', title: 'title3' },
			],
			generatorID,
			groupID: '2',
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="flexForm-select">
				<select
					className="flexForm-select-area"
					id="baseId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					required
					defaultValue="value1"
				>
					<option value="value1">title1</option>
					<option value="value2">value2</option>
					<option value="value3">title3</option>
				</select>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
