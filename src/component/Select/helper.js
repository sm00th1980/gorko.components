import classNames from 'classnames';
import { withoutUndefined } from '../helpers/helper/';

export const selectProps = ({ link, disabled, value, id, groupID, onChange }) => {
	let baseProps = {
		className: 'flexForm-select-area',
		id: `baseId-${link}-${id}`,
		name: `nameId-${link}-${groupID}`,
		'aria-labelledby': `ariaId-${link}-${groupID}`,
		required: true,
	};

	baseProps = disabled ? { ...baseProps, disabled } : baseProps;
	baseProps = onChange
		? { ...baseProps, onChange, value }
		: { ...baseProps, defaultValue: value };

	return withoutUndefined(baseProps);
};

const wrapperKlassName = (klass, small, error) => classNames(
	'flexForm-select',
	{
		typeSmall: small,
		typeError: error,
		[`${klass}`]: klass,
	},
);

export const wrapperProps = ({ klass, small, error, style }) => {
	const baseProps = {
		className: wrapperKlassName(klass, small, error),
		style,
	};

	return withoutUndefined(baseProps);
};
