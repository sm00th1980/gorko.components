/* eslint "react/forbid-prop-types": "off" */

import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { uniqueId } from '../helpers/helper/';
import { wrapperProps, selectProps } from './helper';

export const itemsPropTypes = PropTypes.arrayOf(PropTypes.shape({
	value: PropTypes.string.isRequired,
	title: PropTypes.string,
}));

const option = item => (
	<option key={_.uniqueId()} value={item.value}>{item.title || item.value}</option>
);

const Select = ({
		link, groupID, items, klass, small, error, disabled, value, onChange,
		generatorID = uniqueId, style, children,
	}) => {
	const id = generatorID.next();

	const _wrapperProps = wrapperProps({ klass, small, error, style });
	const _selectProps = selectProps({
		link,
		disabled,
		value: (value || items[0].value),
		id,
		groupID,
		onChange,
	});

	return (
		<div {..._wrapperProps}>
			<select {..._selectProps}>
				{ items.map(option) }
			</select>
			{ children }
		</div>
	);
};

Select.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.array,
	]),
	link: PropTypes.string,
	groupID: PropTypes.string,
	items: itemsPropTypes.isRequired,
	klass: PropTypes.string,
	small: PropTypes.bool,
	error: PropTypes.bool,
	disabled: PropTypes.bool,
	value: PropTypes.string,
	onChange: PropTypes.func,
	generatorID: PropTypes.shape({
		next: PropTypes.func.isRequired,
	}),
	style: PropTypes.object,
};

Select.defaultProps = {
	link: undefined,
	groupID: _.uniqueId(),
	error: false,
	disabled: false,
	klass: '',
	small: false,
	value: undefined,
	onChange: undefined,
	generatorID: uniqueId,
	style: undefined,
};

export default Select;

// examples
// <Select items={SelectData} klass="** widthFlex-200" small link="filter" />
