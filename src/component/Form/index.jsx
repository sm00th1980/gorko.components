/* eslint no-use-before-define: ["error", { "variables": false }] */
/* eslint "react/forbid-prop-types": "off" */

import React, { Children, cloneElement, isValidElement } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { uniqueId, withoutUndefined, isOneOfType, elementNeedLink } from '../helpers/helper/';
import FormGroup from '../FormGroup';

const cloneChild = (groupID, parentLink) => (child, key) => {
	if (isValidElement(child)) {
		const link = isOneOfType([FormGroup], child) && _.isUndefined(parentLink)
			? child.props.link
			: parentLink;

		const children = (child && child.props && child.props.children) || undefined;

		const cloneElementPartial = _.partial(
			cloneElement, child, _, cloneChildren(children, groupID, link),
		);

		if (elementNeedLink(child) || isOneOfType([FormGroup], child)) {
			return cloneElementPartial({ key, groupID, link });
		}

		return cloneElementPartial({ key });
	}

	// non-react element
	return child;
};

const cloneChildren = (children, groupID, link) =>
	Children.map(children, cloneChild(groupID, link));

const Form = ({ children, link, generatorID, klass, style }) => {
	const groupID = generatorID.next();
	const childrenWithKey = Children.map(children, cloneChild(groupID, link));
	const props = withoutUndefined({ className: klass, style });

	return <form {...props}>{childrenWithKey}</form>;
};

Form.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.array,
	]).isRequired,
	link: PropTypes.string,
	generatorID: PropTypes.shape({
		next: PropTypes.func.isRequired,
	}),
	klass: PropTypes.string,
	style: PropTypes.object,
};

Form.defaultProps = {
	link: undefined,
	generatorID: uniqueId,
	klass: undefined,
	style: undefined,
};

export default Form;

//	EXAMPLE
//
//	<Form link="filter">
//		<div>
//			<Input />
//			<Check value="value" />
//		</div>
//	</Form>
