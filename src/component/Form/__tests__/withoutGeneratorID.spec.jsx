import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Form from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Form {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Form/', () => {
	it('should render form without generatorID', () => {
		const props = { link: 'filter', children: <div /> };
		expect(() => create(props)).not.toThrow();
	});
});
