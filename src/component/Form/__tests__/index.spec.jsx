import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Form from '../';
import Input from '../../Input/';
import Check from '../../Check/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Form {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Form/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('1');
	});

	it('should display form with one input inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: <Input link={_.uniqueId()} groupID={_.uniqueId()} />,
		};
		const actualElement = create(props);
		const expectedElement = (
			<form>
				<Input link={props.link} groupID="1" />
			</form>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display form with one div/input inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: <div><Input link={_.uniqueId()} groupID={_.uniqueId()} /></div>,
		};
		const actualElement = create(props);
		const expectedElement = (
			<form>
				<div>
					<Input link={props.link} groupID="1" />
				</div>
			</form>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display form with one div/div/check inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children:
	<div>
		<div>
			<Check value="value" link={_.uniqueId()} groupID={_.uniqueId()} />
		</div>
	</div>,
		};
		const actualElement = create(props);
		const expectedElement = (
			<form>
				<div>
					<div>
						<Check value="value" link={props.link} groupID="1" />
					</div>
				</div>
			</form>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
