import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Form from '../';
import Radio from '../../Radio/';
import FormGroup from '../../FormGroup';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Form {...props} />);
	return renderer.getRenderOutput();
};

describe('component/Form/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('1');
	});

	it('should display form with one formGroup/radio inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children:
	<FormGroup klass="typeColumn" groupID={_.uniqueId()} link={_.uniqueId()}>
		<Radio value="value" groupID={_.uniqueId()} link={_.uniqueId()} />
	</FormGroup>,
		};
		const actualElement = create(props);
		const expectedElement = (
			<form>
				<FormGroup klass="typeColumn" link="filter" groupID="1">
					<Radio value="value" link="filter" groupID="1" />
				</FormGroup>
			</form>
	);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display form with one label_formGroup/radio inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: [
  (<label htmlFor="1">text</label>),
  (<FormGroup klass="typeColumn" groupID={_.uniqueId()} link={_.uniqueId()}>
	<Radio value="value" groupID={_.uniqueId()} link={_.uniqueId()} />
  </FormGroup>),
			],
		};
		const actualElement = create(props);
		const expectedElement = (
			<form>
				<label htmlFor="1">text</label>
				<FormGroup klass="typeColumn" link="filter" groupID="1">
					<Radio value="value" link="filter" groupID="1" />
				</FormGroup>
			</form>
	);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display form with two formGroup/radio inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: [
  (<FormGroup klass="typeColumn" groupID={_.uniqueId()} link={_.uniqueId()} >
	<Radio value="value" groupID={_.uniqueId()} link={_.uniqueId()} />
  </FormGroup>),
  (<FormGroup klass="typeColumn" groupID={_.uniqueId()} link={_.uniqueId()}>
	<Radio value="value" groupID={_.uniqueId()} link={_.uniqueId()} />
  </FormGroup>),
			],
		};

		const actualElement = create(props);
		const expectedElement = (
			<form>
				<FormGroup klass="typeColumn" link="filter" groupID="1">
					<Radio value="value" link="filter" groupID="1" />
				</FormGroup>
				<FormGroup klass="typeColumn" link="filter" groupID="1">
					<Radio value="value" link="filter" groupID="1" />
				</FormGroup>
			</form>
	);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
