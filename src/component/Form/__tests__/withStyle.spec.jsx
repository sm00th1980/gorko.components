import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Form from '../';
import Input from '../../Input/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Form {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Form/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('1');
	});

	it('should display form with style', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: <Input link={_.uniqueId()} groupID={_.uniqueId()} />,
			style: { justifyContent: 'center' },
		};
		const actualElement = create(props);
		const expectedElement = (
			<form style={props.style}>
				<Input link={props.link} groupID="1" />
			</form>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
