import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Form from '../';
import Radio from '../../Radio/';
import FormGroup from '../../FormGroup';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Form {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Form/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('1');
	});

	it('should display form without link and link on formGroup', () => {
		const props = {
			generatorID: groupGeneratorID,
			children:
	<FormGroup klass="typeColumn" groupID={_.uniqueId()} link="filter">
		<Radio value="value" groupID={_.uniqueId()} link={_.uniqueId()} />
	</FormGroup>,
		};
		const actualElement = create(props);
		const expectedElement = (
			<form>
				<FormGroup klass="typeColumn" link="filter" groupID="1">
					<Radio value="value" link="filter" groupID="1" />
				</FormGroup>
			</form>
		);
		expect(actualElement).toEqualJSX(expectedElement);
	});
});
