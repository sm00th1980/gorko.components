import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Form from '../';
import Input from '../../Input/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Form {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Form/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('1');
	});

	it('should display form with one input inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			klass: 'klass',
			children: <Input link={_.uniqueId()} groupID={_.uniqueId()} />,
		};
		const actualElement = create(props);
		const expectedElement = (
			<form className="klass">
				<Input link={props.link} groupID="1" />
			</form>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
