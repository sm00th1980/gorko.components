import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Radio from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Radio {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Radio/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display without value', () => {
		const props = { link: 'filter', generatorID, groupID: '2' };
		const actualElement = create(props);
		const expectedElement = (
			<div className="form-check">
				<input
					type="radio"
					className="form-check-area"
					id="labelId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultChecked={false}
				/>
				<label className="form-check-label" htmlFor="labelId-filter-01" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
