import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Radio from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Radio {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Radio/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display with style', () => {
		const props = {
			value: 'text',
			link: 'filter',
			generatorID,
			groupID: '2',
			style: { justifyContent: 'center' },
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="form-check" style={props.style}>
				<input
					type="radio"
					className="form-check-area"
					id="labelId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultChecked={false}
				/>
				<label className="form-check-label" htmlFor="labelId-filter-01">text</label>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
