import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Radio from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Radio {...props} />);
	return renderer.getRenderOutput();
};

describe('component/Radio/', () => {
	it('should render radio without generatorID', () => {
		const props = { value: 'text', link: 'filter', klass: 'klass', groupID: '2' };
		expect(() => create(props)).not.toThrow();
	});
});
