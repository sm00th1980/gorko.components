import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Radio from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Radio {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Radio/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct value and link', () => {
		const props = { value: 'text', link: 'filter', generatorID, groupID: '2' };
		const actualElement = create(props);
		const expectedElement = (
			<div className="form-check">
				<input
					type="radio"
					className="form-check-area"
					id="labelId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultChecked={false}
				/>
				<label className="form-check-label" htmlFor="labelId-filter-01">text</label>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display checkbox with error', () => {
		const props = { value: 'error', link: 'filter', error: true, generatorID, groupID: '2' };
		const actualElement = create(props);
		const expectedElement = (
			<div className="form-check typeError">
				<input
					type="radio"
					className="form-check-area"
					id="labelId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultChecked={false}
				/>
				<label className="form-check-label" htmlFor="labelId-filter-01">error</label>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display checked checkbox', () => {
		const props = { value: 'text', link: 'filter', checked: true, generatorID, groupID: '2' };
		const actualElement = create(props);
		const expectedElement = (
			<div className="form-check">
				<input
					type="radio"
					className="form-check-area"
					id="labelId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultChecked
				/>
				<label className="form-check-label" htmlFor="labelId-filter-01">text</label>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display disabled checkbox', () => {
		const props = { value: 'disabled', link: 'filter', disabled: true, generatorID, groupID: '2' };
		const actualElement = create(props);
		const expectedElement = (
			<div className="form-check">
				<input
					type="radio"
					className="form-check-area"
					id="labelId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultChecked={false}
					disabled
				/>
				<label className="form-check-label" htmlFor="labelId-filter-01">disabled</label>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
