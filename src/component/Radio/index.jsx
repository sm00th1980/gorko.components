/* eslint "react/forbid-prop-types": "off" */

import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { wrapperKlassName, inputProps, uniqueId, withoutUndefined } from '../helpers/helper/';

const Radio = ({
		link, groupID, value, checked, error, disabled, generatorID = uniqueId,
		klass, onChange, style,
	}) => {
	const id = generatorID.next();

	const props = inputProps('radio')({ link, checked, disabled, id, groupID, onChange });
	const wrapperProps = withoutUndefined({
		className: wrapperKlassName(error, klass),
		style,
	});

	return (
		<div {...wrapperProps}>
			<input {...props} />
			<label className="form-check-label" htmlFor={`labelId-${link}-${id}`}>
				{value}
			</label>
		</div>
	);
};

Radio.propTypes = {
	link: PropTypes.string,
	groupID: PropTypes.string,
	value: PropTypes.string,
	checked: PropTypes.bool,
	error: PropTypes.bool,
	disabled: PropTypes.bool,
	klass: PropTypes.string,
	onChange: PropTypes.func,
	generatorID: PropTypes.shape({
		next: PropTypes.func.isRequired,
	}),
	style: PropTypes.object,
};

Radio.defaultProps = {
	link: undefined,
	groupID: _.uniqueId(),
	checked: false,
	error: false,
	disabled: false,
	value: '',
	klass: '',
	onChange: undefined,
	generatorID: uniqueId,
	style: undefined,
};

export default Radio;

// examples
// <Radio value={t('Фотографы')} link='filter' disabled checked />
