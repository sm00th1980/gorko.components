/* eslint "react/forbid-prop-types": "off" */

import React, { Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { withoutUndefined } from '../helpers/helper/';

const cloneChild = (child, key) => cloneElement(child, { key });

const BtnGroup = ({ children, style }) => {
	const childrenWithKey = Children.map(children, cloneChild);

	const props = withoutUndefined({ className: 'btn-group', style });
	return <div {...props}>{childrenWithKey}</div>;
};

BtnGroup.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.array,
	]).isRequired,
	style: PropTypes.object,
};

BtnGroup.defaultProps = {
	style: undefined,
};

export default BtnGroup;

//	EXAMPLE
//
//	<BtnGroup>
//		<Btn value={t('Показать')} klass='btn-small btn-green-f ** width-220' />
//	</BtnGroup>
