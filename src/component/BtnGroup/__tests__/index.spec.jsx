import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import BtnGroup from '../';
import Btn from '../../Btn/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<BtnGroup {...props} />);

	return renderer.getRenderOutput();
};

describe('component/BtnGroup/', () => {
	it('should display button-group with one button', () => {
		const props = { children: <Btn value="value" klass="klass" /> };
		const actualElement = create(props);
		const expectedElement = (
			<div className="btn-group">
				<Btn value="value" klass="klass" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display button-group with two buttons', () => {
		const props = {
			children: [
				(<Btn value="value1" klass="klass1" />),
				(<Btn value="value2" klass="klass2" />),
			],
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="btn-group">
				<Btn value="value1" klass="klass1" />
				<Btn value="value2" klass="klass2" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
