import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import BtnGroup from '../';
import Btn from '../../Btn/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<BtnGroup {...props} />);

	return renderer.getRenderOutput();
};

describe('component/BtnGroup/', () => {
	it('should display button-group with style', () => {
		const props = {
			children: <Btn value="value" klass="klass" />,
			style: { justifyContent: 'center' },
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="btn-group" style={props.style}>
				<Btn value="value" klass="klass" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
