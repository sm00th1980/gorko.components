import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import FormGroup from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<FormGroup {...props} />);

	return renderer.getRenderOutput();
};

describe('component/FormGroup/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('01');
	});

	it('should display form-group with style', () => {
		const props = {
			style: { justifyContent: 'center' },
			children: <div />,
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="flexForm-group" style={props.style}>
				<div />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
