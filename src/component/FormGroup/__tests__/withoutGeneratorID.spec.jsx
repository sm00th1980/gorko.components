import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import FormGroup from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<FormGroup {...props} />);

	return renderer.getRenderOutput();
};

describe('component/FormGroup/', () => {
	it('should render formGroup without generatorID', () => {
		const props = { link: 'filter',	children: <span /> };
		expect(() => create(props)).not.toThrow();
	});
});
