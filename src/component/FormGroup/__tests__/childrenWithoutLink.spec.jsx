import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Radio from '../../Radio/';
import FormGroup from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<FormGroup {...props} />);

	return renderer.getRenderOutput();
};

describe('component/FormGroup/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('1');
	});

	it('should display form with one formGroup/radio_span inside', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: [
				(<Radio value="value" groupID={_.uniqueId()} link={_.uniqueId()} />),
				(<span>text</span>),
			],
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="flexForm-group">
				<Radio value="value" link="filter" groupID="1" />
				<span>text</span>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
