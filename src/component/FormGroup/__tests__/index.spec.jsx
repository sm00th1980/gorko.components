import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import FormGroup from '../';
import Check from '../../Check/';
import Input from '../../Input/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<FormGroup {...props} />);

	return renderer.getRenderOutput();
};

describe('component/FormGroup/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('01');
	});

	it('should display form-group with one check', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: <Check value="value" link={_.uniqueId()} groupID={_.uniqueId()} />,
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="flexForm-group">
				<Check value="value" link={props.link} groupID="01" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display form-group with one input and one check', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			children: [
				(<Check value="value" link={_.uniqueId()} groupID={_.uniqueId()} />),
				(<Input link={_.uniqueId()} groupID={_.uniqueId()} />),
			],
		};

		const actualElement = create(props);
		const expectedElement = (
			<div className="flexForm-group">
				<Check value="value" link={props.link} groupID="01" />
				<Input link={props.link} groupID="01" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
