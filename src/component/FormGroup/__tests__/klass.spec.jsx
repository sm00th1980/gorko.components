import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import FormGroup from '../';
import Check from '../../Check/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<FormGroup {...props} />);

	return renderer.getRenderOutput();
};

describe('component/FormGroup/', () => {
	let groupGeneratorID;

	beforeEach(() => {
		groupGeneratorID = { next: _.noop };
		spyOn(groupGeneratorID, 'next').and.returnValues('1');
	});

	it('should display form-group with one check', () => {
		const props = {
			link: 'filter',
			generatorID: groupGeneratorID,
			klass: 'klass',
			children: <Check value="value" link={_.uniqueId()} groupID={_.uniqueId()} />,
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className={`flexForm-group ${props.klass}`}>
				<Check value="value" link={props.link} groupID="1" />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
