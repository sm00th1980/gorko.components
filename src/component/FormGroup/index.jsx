/* eslint "react/forbid-prop-types": "off" */

import React, { Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from 'classnames';
import {
	uniqueId,
	elementNeedLink,
	filterChildren,
	withoutUndefined,
} from '../helpers/helper/';

const FormGroup = ({ children, link, generatorID, klass, groupID, style }) => {
	const groupID_ = groupID || generatorID.next();
	const cloneChild = () => (child, key) => {
		const _cloneElement = _.partial(cloneElement, child);
		return elementNeedLink(child)
			? _cloneElement({ key, groupID: groupID_, link })
			: _cloneElement({ key });
	};

	const childrenWithKey = Children.map(filterChildren(children), cloneChild());

	const className = classNames('flexForm-group', { [`${klass}`]: klass });
	const props = withoutUndefined({ className, style });

	return <div {...props}>{childrenWithKey}</div>;
};

FormGroup.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.array,
	]).isRequired,
	link: PropTypes.string,
	generatorID: PropTypes.shape({
		next: PropTypes.func.isRequired,
	}),
	klass: PropTypes.string,
	groupID: PropTypes.string,
	style: PropTypes.object,
};

FormGroup.defaultProps = {
	link: undefined,
	generatorID: uniqueId,
	klass: undefined,
	groupID: undefined,
	style: undefined,
};

export default FormGroup;

//	EXAMPLE
//
//	<FormGroup link="filter">
//		<Input />
//		<Check value="value" />
//	</FormGroup>
