/* eslint "jsx-a11y/no-static-element-interactions": "off" */
/* eslint "react/no-danger": "off" */

import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Btn from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Btn {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Btn/', () => {
	it('should display button with style', () => {
		const props = { value: 'Show', klass: 'btn-small', style: { justifyContent: 'center' } };
		const actualElement = create(props);
		const expectedElement = (
			<div className={props.klass} role="button" tabIndex="0" style={props.style}>
				<span className="btn-value" dangerouslySetInnerHTML={{ __html: props.value }} />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
