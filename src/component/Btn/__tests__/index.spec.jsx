/* eslint "jsx-a11y/no-static-element-interactions": "off" */
/* eslint "react/no-danger": "off" */

import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Btn from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Btn {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Btn/', () => {
	it('should display button with value and klass', () => {
		const props = { value: 'Show', klass: 'btn-small' };
		const actualElement = create(props);
		const expectedElement = (
			<div className={props.klass} role="button" tabIndex="0">
				<span className="btn-value" dangerouslySetInnerHTML={{ __html: props.value }} />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display button with value and klass and action', () => {
		const props = { value: 'Show', klass: 'btn-small', onClick: _.noop };
		const actualElement = create(props);
		const expectedElement = (
			<div className={props.klass} role="button" tabIndex="0" onClick={props.onClick} >
				<span className="btn-value" dangerouslySetInnerHTML={{ __html: props.value }} />
			</div>
	);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
