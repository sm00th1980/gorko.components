import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Btn, { MODE, WAITING_TEXT } from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Btn {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Btn/', () => {
	it('should display button in waiting mode', () => {
		const props = { klass: 'btn-small', mode: MODE.WAITING };
		const actualElement = create(props);
		const expectedElement = (
			<div className={`${props.klass} btn-gray-f`} role="button" tabIndex="-1">
				<span className="btn-value">{ WAITING_TEXT }</span>
				<div className="widget-loaderNEW-wrap size17">
					<i className="widget-loaderNEW ** icon-loader" />
				</div>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
