/* eslint "react/no-danger": "off" */

import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Btn, { MODE } from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Btn {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Btn/', () => {
	it('should display button in disabled mode', () => {
		const props = { value: 'Show', klass: 'btn-small', mode: MODE.DISABLED };
		const actualElement = create(props);
		const expectedElement = (
			<div className={`${props.klass} itemNonActive`} role="button" tabIndex="-1">
				<span className="btn-value" dangerouslySetInnerHTML={{ __html: props.value }} />
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
