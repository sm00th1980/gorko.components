/* eslint "react/no-danger": "off" */
/* eslint "react/forbid-prop-types": "off" */

import React from 'react';
import PropTypes from 'prop-types';
import { withoutUndefined } from '../helpers/helper/';
import Helper, { OCTET } from '../helpers/customClass/';

export const WAITING_TEXT = 'Ожидайте...';

export const MODE = {
	NORMAL: 'normal',
	DISABLED: 'disabled',
	WAITING: 'waiting',
};

const btnNormal = (value, klass, onClick, style) => {
	const props = withoutUndefined({
		className: klass,
		onClick,
		role: 'button',
		tabIndex: '0',
		style,
	});

	return (
		<div {...props} >
			<span className="btn-value" dangerouslySetInnerHTML={{ __html: value }} />
		</div>
	);
};

const btnDisabled = (value, klass, style) => {
	const props = withoutUndefined({
		className: Helper.withSpecialKlass(klass, 'itemNonActive', OCTET.FIRST),
		role: 'button',
		tabIndex: '-1',
		style,
	});

	return (
		<div {...props} >
			<span className="btn-value" dangerouslySetInnerHTML={{ __html: value }} />
		</div>
	);
};

const btnWaiting = (klass, style) => {
	const props = withoutUndefined({
		className: Helper.withSpecialKlass(klass, 'btn-gray-f', OCTET.FIRST),
		role: 'button',
		tabIndex: '-1',
		style,
	});

	return (
		<div {...props} >
			<span className="btn-value">{ WAITING_TEXT }</span>
			<div className="widget-loaderNEW-wrap size17">
				<i className="widget-loaderNEW ** icon-loader" />
			</div>
		</div>
	);
};

const Btn = ({ value, klass, onClick, mode, style }) => {
	switch (mode) {
	case MODE.WAITING:
		return btnWaiting(klass, style);
	case MODE.DISABLED:
		return btnDisabled(value, klass, style);
	default:
		return btnNormal(value, klass, onClick, style);
	}
};

Btn.propTypes = {
	klass: PropTypes.string.isRequired,
	value: PropTypes.string,
	onClick: PropTypes.func,
	mode: PropTypes.oneOf([MODE.NORMAL, MODE.DISABLED, MODE.WAITING]),
	style: PropTypes.object,
};

Btn.defaultProps = {
	onClick: undefined,
	mode: MODE.NORMAL,
	style: undefined,
};

export default Btn;

//	EXAMPLE
//
//	<Btn
//		value={t('Показать')}
//		klass='btn-small btn-green-f ** width-220' />
//	<Btn
//		value={t('Показать')}
//		klass={classNames('btn-small btn-green-f **', {'width-220': true})} />
//	<Btn
//		value={t('Показать')}
//		klass={classNames('btn-small btn-green-f', {'** width-220': true})} />
