/* eslint "react/forbid-prop-types": "off" */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withoutUndefined } from '../helpers/helper/';

const Avatar = ({ user, style }) => {
	const classes = classNames('widget-avatar-wrap size30 typeBlack textSize170', {
		typePRO: !!user.top,
	});

	const props = withoutUndefined({ className: classes, style });

	return (
		<div {...props} >
			<a className="widget-avatar-img-link">
				<img className="widget-avatar-img" src={user.avatar_url} alt={user.name} />
			</a>
			<div className="widget-avatar-info">
				<a className="widget-avatar-name" href="">{ user.name }</a>
			</div>
		</div>
	);
};

Avatar.propTypes = {
	user: PropTypes.shape({
		name: PropTypes.string,
		avatar_url: PropTypes.string,
	}).isRequired,
	style: PropTypes.object,
};

Avatar.defaultProps = {
	style: undefined,
};

export default Avatar;

