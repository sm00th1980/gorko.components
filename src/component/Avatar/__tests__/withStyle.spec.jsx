import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Avatar from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Avatar {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Avatar', () => {
	it('should display avatar with style', () => {
		const name = 'User Name';
		const avatarUrl = '/future/static/img/cap/avatar/avatar—noauth.svg';
		const props = {
			style: { justifyContent: 'center' },
			user: { name, avatar_url: avatarUrl },
		};
		const actualElement = create(props);
		const expectedElement = (
			<div className="widget-avatar-wrap size30 typeBlack textSize170" style={props.style}>
				<a className="widget-avatar-img-link">
					<img className="widget-avatar-img" src={avatarUrl} alt={name} />
				</a>
				<div className="widget-avatar-info">
					<a className="widget-avatar-name" href="">{name}</a>
				</div>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
