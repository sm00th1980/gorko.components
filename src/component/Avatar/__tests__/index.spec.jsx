import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Avatar from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Avatar {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Avatar', () => {
	it('should display correct name and avatar url', () => {
		const name = 'User Name';
		const avatarUrl = '/future/static/img/cap/avatar/avatar—noauth.svg';
		const props = { user: { name, avatar_url: avatarUrl } };
		const actualElement = create(props);
		const expectedElement = (
			<div className="widget-avatar-wrap size30 typeBlack textSize170">
				<a className="widget-avatar-img-link">
					<img className="widget-avatar-img" src={avatarUrl} alt={name} />
				</a>
				<div className="widget-avatar-info">
					<a className="widget-avatar-name" href="">{name}</a>
				</div>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct name, avatar url and class name for top user ', () => {
		const name = 'User Name';
		const avatarUrl = '/future/static/img/cap/avatar/avatar—noauth.svg';
		const props = { user: { name, avatar_url: avatarUrl, top: 1 } };
		const actualElement = create(props);
		const expectedElement = (
			<div className="widget-avatar-wrap size30 typeBlack textSize170 typePRO">
				<a className="widget-avatar-img-link">
					<img className="widget-avatar-img" src={avatarUrl} alt={name} />
				</a>
				<div className="widget-avatar-info">
					<a className="widget-avatar-name" href="">{name}</a>
				</div>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
