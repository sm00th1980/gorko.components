import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Check from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Check {...props} />);

	return renderer.getRenderOutput();
};

describe('component/Check/', () => {
	let generatorID;
	beforeEach(() => {
		generatorID = { next: _.noop };
		spyOn(generatorID, 'next').and.returnValues('01');
	});

	it('should display correct value and link and klass', () => {
		const props = { value: 'text', link: 'filter', generatorID, klass: 'klass', groupID: '2' };
		const actualElement = create(props);
		const expectedElement = (
			<div className="form-check klass">
				<input
					type="checkbox"
					className="form-check-area"
					id="labelId-filter-01"
					name={`nameId-filter-${props.groupID}`}
					aria-labelledby={`ariaId-filter-${props.groupID}`}
					defaultChecked={false}
				/>
				<label className="form-check-label" htmlFor="labelId-filter-01">text</label>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
