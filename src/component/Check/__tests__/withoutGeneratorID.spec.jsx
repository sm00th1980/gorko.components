import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import Check from '../';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Check {...props} />);
	return renderer.getRenderOutput();
};

describe('component/Check/', () => {
	it('should render radio without generatorID', () => {
		const props = { value: 'text', link: 'filter', groupID: '2' };
		expect(() => create(props)).not.toThrow();
	});
});
