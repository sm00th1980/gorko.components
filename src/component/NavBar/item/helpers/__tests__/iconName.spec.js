import _ from 'lodash';
import iconClass, { ICON, KLASS, DEFAULT_KLASS } from '../iconName';

describe('Helper NavBar item - .iconClass()', () => {
	it('should return empty string for invalid keys', () => {
		const invalidKeyNames = [
			'',
			'qwerty',
			undefined,
			null,
			NaN,
			true,
			false,
			Infinity,
			-Infinity,
		];

		_.each(invalidKeyNames, (invalidName) => {
			const result = iconClass(invalidName);
			expect(result).toEqual(DEFAULT_KLASS);
		});
	});

	it('should return correct class for icon with star', () => {
		const result = iconClass(ICON.STAR);
		expect(result).toEqual(KLASS.STAR);
	});

	it('should return correct class for icon with arrow', () => {
		const result = iconClass(ICON.ARROW);
		expect(result).toEqual(KLASS.ARROW);
	});
});
