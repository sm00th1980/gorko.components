export const ICON = { STAR: 'STAR', ARROW: 'ARROW' };
export const KLASS = {
	STAR: 'widget-iconStar widget-iconWithText size20 ** icon-star',
	ARROW: 'ic-arrow size12 itemDirR widget-iconWithText ** icon-arrow',
};
export const DEFAULT_KLASS = '';

export default iconName => KLASS[iconName] || DEFAULT_KLASS;
