import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import SimpleItem from './simple';
import IconItem from './icon';
import FilterItem from './filter';

export const onClickHandler = (action, fromKey, actionByName, evt) => {
	evt.preventDefault();
	evt.stopPropagation();

	actionByName(action)({ el: evt.target, fromKey });
};

const Item = (props) => {
	const { type, icon } = props;
	const propsNormalized = _.omit(props, ['actionByName', 'fromKey']);

	if (type === 'filter') {
		return <FilterItem onClick={props.onClick} title={props.title} />;
	} else if (icon) {
		return <IconItem {... propsNormalized} />;
	}

	const onClick = _.partial(onClickHandler, props.action, props.fromKey, props.actionByName);
	return <SimpleItem {...propsNormalized} onClick={onClick} />;
};

Item.propTypes = {
	type: PropTypes.string,
	icon: PropTypes.string,
	title: PropTypes.string,
	action: PropTypes.string,
	fromKey: PropTypes.shape({}),
	actionByName: PropTypes.func,
	onClick: PropTypes.func,
};

Item.defaultProps = {
	type: '',
	icon: '',
	title: '',
	action: '',
	fromKey: {},
	actionByName: _.noop,
	onClick: _.noop,
};

export default Item;
