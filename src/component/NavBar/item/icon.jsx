import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import iconName from './helpers/iconName';

const ItemIcon = ({ active, title, url, icon, defaultUrl, onClick }) => (
	<li>
		<a
			className={classNames('widget-iconWithText-wrap', 'subMenu-link', 'link-blue', '**', { itemActive: active })}
			href={url || defaultUrl}
			onClick={onClick}
		>
			<i className={iconName(icon)} />
			<span className="widget-iconWithText-value">{ title }</span>
		</a>
	</li>
);

ItemIcon.propTypes = {
	active: PropTypes.bool,
	title: PropTypes.string.isRequired,
	url: PropTypes.string,
	icon: PropTypes.string,
	defaultUrl: PropTypes.string,
	onClick: PropTypes.func.isRequired,
};

ItemIcon.defaultProps = {
	active: false,
	url: '',
	defaultUrl: '',
	icon: '',
};

export default ItemIcon;
