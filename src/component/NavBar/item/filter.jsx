/* eslint "jsx-a11y/no-static-element-interactions": "off" */
import React from 'react';
import PropTypes from 'prop-types';

const ItemFilter = ({ title, onClick }) => (
	<li>
		<div className="subMenu-link link-gray" onClick={onClick} >{ title }</div>
	</li>
);

ItemFilter.propTypes = {
	title: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
};

export default ItemFilter;
