import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const ItemSimple = ({ active, title, url, defaultUrl, onClick }) => (
	<li>
		<a
			className={classNames('subMenu-link', 'link-blue', '**', { itemActive: active })}
			href={url || defaultUrl}
			onClick={onClick}
		>{ title }</a>
	</li>
	);

ItemSimple.propTypes = {
	active: PropTypes.bool,
	title: PropTypes.string.isRequired,
	url: PropTypes.string,
	defaultUrl: PropTypes.string,
	onClick: PropTypes.func.isRequired,
};

ItemSimple.defaultProps = {
	active: false,
	url: '',
	defaultUrl: '',
};

export default ItemSimple;
