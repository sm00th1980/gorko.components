import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';
import Item from './item/';

const NavBar = ({ data, additionalKlass, fromKey, actionByName }) => {
	const items = data.map(props => (
		<Item fromKey={fromKey} actionByName={actionByName} {...props} />),
	);

	const klass = classNames(
		'subMenu',
		'typeUserMenu',
		'**',
		{ [additionalKlass]: !!additionalKlass },
		{ typeShort: data.length < 4 },
	);

	return (
		<div className="flex-wrap-bg typeOverflow">
			<div className="flex-wrap typeWide">
				<ul className={klass}>
					{ items }
				</ul>
			</div>
		</div>
	);
};

NavBar.propTypes = {
	data: PropTypes.arrayOf(PropTypes.shape({
		action: PropTypes.string,
		active: PropTypes.bool,
		title: PropTypes.string.isRequired,
		url: PropTypes.string,
		defaultUrl: PropTypes.string,
		icon: PropTypes.string,
		type: PropTypes.string,
	})).isRequired,
	additionalKlass: PropTypes.string,
	fromKey: PropTypes.shape({}),
	actionByName: PropTypes.func,
};

NavBar.defaultProps = {
	additionalKlass: '',
	fromKey: {},
	actionByName: _.noop,
};

export default NavBar;
