import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import _ from 'lodash';
import Item from '../item/';
import ItemSimple from '../item/simple';
import ItemIcon from '../item/icon';
import ItemFilter from '../item/filter';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<Item {...props} />);

	return renderer.getRenderOutput();
};

describe('component/NavBar/item', () => {
	it('should display correct simple component as default', () => {
		const onClick = _.noop;
		const props = {
			active: true,
			key: 'k1',
			title: 'title1',
			url: '/url/test1/',
			action: 'ACTION1',
			info: '',
			onClick,
		};
		const actualElement = create(props);

		const expectedElement = (
			<ItemSimple
				action="ACTION1"
				active
				defaultUrl=""
				icon=""
				info=""
				onClick={onClick}
				title="title1"
				type=""
				url="/url/test1/"
			/>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct component for item with icon', () => {
		const props = {
			active: true,
			icon: 'icon.svg',
			key: 'k1',
			title: 'title1',
			url: '/url/test1/',
			action: 'ACTION1',
			info: '',
			onClick: _.noop,
		};
		const actualElement = create(props);

		const expectedElement = (
			<ItemIcon
				action="ACTION1"
				active
				defaultUrl=""
				icon="icon.svg"
				info=""
				onClick={_.noop}
				title="title1"
				type=""
				url="/url/test1/"
			/>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct component for item filter', () => {
		const props = {
			type: 'filter',
			title: 'title1',
			onClick: _.noop,
		};
		const actualElement = create(props);

		const expectedElement = <ItemFilter	onClick={props.onClick} title={props.title} />;

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
