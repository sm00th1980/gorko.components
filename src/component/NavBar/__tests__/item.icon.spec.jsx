import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import ItemIcon from '../item/icon';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<ItemIcon {...props} />);

	return renderer.getRenderOutput();
};

describe('component/NavBar/item/icon', () => {
	it('should display correct icon component structure', () => {
		const props = { key: 'k1', title: 'title1', url: '/url/test1/', action: 'ACTION1', info: '', onClick: function onClickHandler() {} };
		const actualElement = create(props);

		const expectedElement = (
			<li>
				<a
					className="widget-iconWithText-wrap subMenu-link link-blue **"
					href="/url/test1/"
					onClick={function onClickHandler() {}}
				>
					<i className="" />
					<span className="widget-iconWithText-value">title1</span>
				</a>
			</li>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct icon component structure for active', () => {
		const props = { active: true, key: 'k1', title: 'title1', url: '/url/test1/', action: 'ACTION1', info: '', onClick: function onClickHandler() {} };
		const actualElement = create(props);

		const expectedElement = (
			<li>
				<a
					className="widget-iconWithText-wrap subMenu-link link-blue ** itemActive"
					href="/url/test1/"
					onClick={function onClickHandler() {}}
				>
					<i className="" />
					<span className="widget-iconWithText-value">title1</span>
				</a>
			</li>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
