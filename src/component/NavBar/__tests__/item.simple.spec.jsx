import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import ItemSimple from '../item/simple';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<ItemSimple {...props} />);

	return renderer.getRenderOutput();
};

describe('component/NavBar/item/simple', () => {
	it('should display correct simple component structure', () => {
		const props = { key: 'k1', title: 'title1', url: '/url/test1/', action: 'ACTION1', info: '', onClick: function onClickHandler() {} };
		const actualElement = create(props);

		const expectedElement = (
			<li>
				<a
					className="subMenu-link link-blue **"
					href="/url/test1/"
					onClick={function onClickHandler() {}}
				>title1</a>
			</li>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct simple component structure for active', () => {
		const props = { active: true, key: 'k1', title: 'title1', url: '/url/test1/', action: 'ACTION1', info: '', onClick: function onClickHandler() {} };
		const actualElement = create(props);

		const expectedElement = (
			<li>
				<a
					className="subMenu-link link-blue ** itemActive"
					href="/url/test1/"
					onClick={function onClickHandler() {}}
				>title1</a>
			</li>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
