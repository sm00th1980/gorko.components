/* eslint "jsx-a11y/no-static-element-interactions": "off" */
import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import ItemFilter from '../item/filter';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<ItemFilter {...props} />);

	return renderer.getRenderOutput();
};

describe('component/NavBar/item/filter', () => {
	it('should display correct filter component structure', () => {
		const props = { key: 'k1', title: 'title1', action: 'ACTION1', info: '', onClick: function onClickHandler() {} };
		const actualElement = create(props);

		const expectedElement = (
			<li>
				<div className="subMenu-link link-gray" onClick={function onClickHandler() {}} >title1</div>
			</li>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
