import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';
import NavBar from '../';
import Item from '../item/';

const create = (props) => {
	const renderer = createRenderer();
	renderer.render(<NavBar {...props} />);

	return renderer.getRenderOutput();
};

describe('component/NavBar', () => {
	it('should display correct structure', () => {
		const NavBarData = [
			{ active: true, key: 'k1', title: 'title1', url: '/url/test1/', action: 'ACTION1', info: '' },
			{ active: false, key: 'k2', title: 'title2', url: '/url/test2/', action: 'ACTION2', info: '' },
			{ active: false, key: 'k3', title: 'title3', url: '/url/test3/', action: 'ACTION3', info: '' },
			{ active: false, key: 'k4', title: 'title4', url: '/url/test4/', action: 'ACTION4', info: '' },
			{ active: false, key: 'k5', title: 'title5', url: '/url/test5/', action: 'ACTION5', info: '' },
		];

		const props = { data: NavBarData };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flex-wrap-bg typeOverflow">
				<div className="flex-wrap typeWide">
					<ul className="subMenu typeUserMenu **">
						<Item {...NavBarData[0]} />
						<Item {...NavBarData[1]} />
						<Item {...NavBarData[2]} />
						<Item {...NavBarData[3]} />
						<Item {...NavBarData[4]} />
					</ul>
				</div>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct structure for short version', () => {
		const NavBarData = [
			{ active: true, key: 'k1', title: 'title1', url: '/url/test1/', action: 'ACTION1', info: '' },
			{ active: false, key: 'k2', title: 'title2', url: '/url/test2/', action: 'ACTION2', info: '' },
			{ active: false, key: 'k3', title: 'title3', url: '/url/test3/', action: 'ACTION3', info: '' },
		];

		const props = { data: NavBarData };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flex-wrap-bg typeOverflow">
				<div className="flex-wrap typeWide">
					<ul className="subMenu typeUserMenu ** typeShort">
						<Item {...NavBarData[0]} />
						<Item {...NavBarData[1]} />
						<Item {...NavBarData[2]} />
					</ul>
				</div>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});

	it('should display correct structure with addition class', () => {
		const NavBarData = [
			{ active: true, key: 'k1', title: 'title1', url: '/url/test1/', action: 'ACTION1', info: '' },
			{ active: false, key: 'k2', title: 'title2', url: '/url/test2/', action: 'ACTION2', info: '' },
			{ active: false, key: 'k3', title: 'title3', url: '/url/test3/', action: 'ACTION3', info: '' },
			{ active: false, key: 'k4', title: 'title4', url: '/url/test4/', action: 'ACTION4', info: '' },
			{ active: false, key: 'k5', title: 'title5', url: '/url/test5/', action: 'ACTION5', info: '' },
		];

		const props = { data: NavBarData, additionalKlass: 'typeUserMenu' };
		const actualElement = create(props);

		const expectedElement = (
			<div className="flex-wrap-bg typeOverflow">
				<div className="flex-wrap typeWide">
					<ul className="subMenu typeUserMenu ** typeUserMenu">
						<Item {...NavBarData[0]} />
						<Item {...NavBarData[1]} />
						<Item {...NavBarData[2]} />
						<Item {...NavBarData[3]} />
						<Item {...NavBarData[4]} />
					</ul>
				</div>
			</div>
		);

		expect(actualElement).toEqualJSX(expectedElement);
	});
});
