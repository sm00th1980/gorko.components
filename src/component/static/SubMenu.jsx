/* eslint no-script-url: "off" */

import React, { PropTypes } from 'react';

const item = 'subMenu-link link-blue';
const itemActiveKlass = 'itemActive';
const itemActiveFn = active => (active ? `${item} ${itemActiveKlass}` : item);

const itemWrap = (text, active = false) => (
	<li key={_.uniqueId()}>
		<a className={itemActiveFn(active)} href="javascript:void(0);">
			{t(text)}
		</a>
	</li>
);

const SubMenu = ({ data }) => (
	<div className="flex-wrap-bg typeOverflow">
		<div className="flex-wrap typeWide">
			<ul className="subMenu typeUserMenu">
				{data.map(i => itemWrap(i.value, i.active))}
			</ul>
		</div>
	</div>
);

SubMenu.propTypes = {
	data: PropTypes.arrayOf(PropTypes.shape({
		value: PropTypes.string.isRequired,
		active: PropTypes.bool,
	})).isRequired,
};

export default SubMenu;

//	EXAMPLE
//
//	<SubMenu data={SubMenuData} />
